/* CSS */
import './assets/css/bootstrap.min.css'
import './assets/css/loading.css'
import './assets/css/loading-btn.css'
import './assets/css/style.css'
import './assets/css/normalize.css'
import './assets/css/ion.rangeSlider.css'

/* JS */
import './assets/js/jquery-3.1.1.min.js'
import './assets/js/bootstrap.bundle.min.js'
import './assets/js/jquery.bxslider.js'
import './assets/js/ion.rangeSlider.js'

/* Vue app */
import Vue from 'vue'
import App from './App.vue'

/* Vue dependencies */
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'

/* Routes */
import Home from './components/Home.vue'

Vue.use(VueResource)
Vue.use(VueRouter)

Vue.http.interceptors.push(function(request, next) {
  request.url = `http://time.mirea.me${request.url}`
  next(response => {
    if(response.status != 200) {
      alert('Whoops, an unknown error acquired.')
    }

    return response
  })
})

new Vue({
  el: '#app',
  components: { App },
  router: new VueRouter({
    routes: [
      { path: '/', component: Home }
    ]
  })
})